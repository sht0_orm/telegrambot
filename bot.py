import random
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

print("Бот запущен. Нажмите Ctrl+C для завершения")

def on_start(update, context):
	chat = update.effective_chat
	context.bot.send_message(chat_id=chat.id, text="С наступающим, бро! Введи свое имя")

def remove_string(filename, string):
    rst = []
    with open(filename) as fd:
        t = fd.read()
        for line in t.splitlines():
            if line != string:
                rst.append(line)
    with open(filename, 'w') as fd:
        fd.write('\n'.join(rst))


def random_costume():
    with open("custume.txt", 'r') as file:
        list_custume = file.read().split("\n")
    print(list_custume)
    random_index = random.randint(0, len(list_custume) - 1)
    costume = list_custume[random_index]
    remove_string('custume.txt', costume)
    return(costume)

def write_name_costume(text, message):
    file = open("name.txt", "a")
    file.write(str(text) + " - " + message + '\n')
    file.close()

def search (text):
    i=0
    with open("name.txt") as file:
        for line in file:
            list_words = line.split()
            i=i+1
            if text in list_words:
                your_costume = line
                break
            else:
                your_costume = "no"
    return(your_costume)

def photo_costume(text):
    photo1 = open('photo/error.jpg','rb')
    if text == 'Пчела':
        photo1 = open('photo/pchel.jpg','rb')
    else:
        if text == 'Мужик':
            photo1 = open('photo/muzhik.jpg','rb')
        else:
            if text == 'Баба':
                photo1 = open('photo/baba.jpg','rb')
            else:
                if text == 'Моряк':
                    photo1 = open('photo/moryak.jpg','rb')
                else:
                    if text == 'Медведь':
                        photo1 = open('photo/medved.jpg','rb')
                    else:
                        if text == 'Командор':
                            photo1 = open('photo/comandor.jpg','rb')
                        else:
                           if text == 'Бурпл':
                               photo1 = open('photo/burpl.jpg','rb')
                           else:
                               if text == 'Бармен':
                                   photo1 = open('photo/barmen.jpg','rb')
                               else:
                                   if text == 'Дринкинс':
                                       photo1 = open('photo/drinkins.jpg','rb')
    return(photo1)

def description(text):
    global desc
    desc = "Э, долбаеб, костюмы кончились!!!))) Махаца бушь? До крови???"
    if text == 'Пчела':
        desc = "Огроменная пчела, которая живёт в улье во дворе. Иногда выполняет роль сторожевого пса, а иногда — почтальона (её улей также служит почтовым ящиком). Довольно агрессивная, запросто может напасть даже на хозяев, но чаще всего от её зубов страдает медведь, который регулярно пытается украсть мёд (впрочем, он тоже не дурак дать пчеле сдачи, кулаками)"
    else:
        if text == 'Мужик':
            desc = "Главный герой рубрики Деревня дураков. Мужик постоянно пытается выкрасть самогон из-под носа у Бабы, но она всё это замечает и в наказание бьёт его по голове сковородой. У Мужика есть свои хобби: стрельба из ружья, охота, рыбалка"
        else:
            if text == 'Баба':
                desc = "Жена Мужика. Мужик постоянно пытается выкрасть самогон из-под носа у Бабы, но она всё это замечает и в наказание бьёт его по голове сковородой"
            else:
                if text == 'Моряк':
                    desc = "Друг и собутыльник Мужика, тоже пьяница. Морячок-дурачок часто заходит в гости к Мужику и Бабе, как к себе в дом"
                else:
                    if text == 'Медведь':
                        desc = "Говорящий медведь, немного глуповат, но добродушен. Постоянно норовит украсть мёд из пчелиного улья (возле дома Мужика и Бабы). За что получает удар сковородкой от бабы, либо заряд соли от Мужика, либо бывает покусан пчелой. Получая урон, Медведь издаёт тонкий пронзительный собачий визг"
                    else:
                        if text == 'Командор':
                            desc = "Неунывающий и мужественный первый пилот — любитель чёрного юмора. Постоянно пытается напугать Дринкинса своими шуточками из разряда мы все умрём. При этом Командор дико ржёт над собственными же шутками на протяжении всего сериала.Каждая серия заканчивается тем, что Командор спрашивает, сколько осталось серий до конца, и шутит в духе Тогда я ещё успею… (вспомнить детство, почувствовать себя японцем и т. п.). Затем следует гомерический хохот Командора и титры"
                        else:
                            if text == 'Бурпл':
                                desc = "Несравненная мисс Бурпл, о которой все говорят, но которая появляется, если не считать титров, только в 28-й серии.Толстая рыжая тётка, основной источник проблем в пассажирском салоне"
                            else:
                                if text == 'Бармен':
                                    desc = "Тощий мужичок с усиками, в бабочке и с хитроватым взглядом. В основном стоит за стойкой. Возле стойки сидит Пьяница и непрерывно пьёт напитки, которые ему наливает Бармен (именно с этого момента и начинается каждый выпуск)"
                                else:
                                    if text == 'Дринкинс':
                                        desc = "Изобретательный и находчивый второй пилот. Боится смерти, пытается как-то спасти ситуацию, предлагая выпустить шасси, подать сигнал SOS и т. д, чем только забавляет Командора. Появление Стюардессы и радиста Командор обычно сопровождает недоуменным вопросом к Дринкинсу: Кто это, Дринкинс? (англ. Who is it, Drinkins?) на что Дринкинс сначала (в ранних сериях) отвечает правильно, а потом начинает выдумывать (ему надоело отвечать одно и то же)"
    return(desc)

def on_message(update, context):
    chat = update.effective_chat
    text = update.message.text
    if search(text) == "no":
        message = random_costume()
        costume = message
        write_name_costume(text, message)
        get_photo = photo_costume(costume)
        desc = description(costume)
    else:
        message = "Мррррразь, ты уже выбрал костюм!"
    context.bot.send_message(chat_id=chat.id, text="Твой костюм - " + message + "\nУдачи!))")
    context.bot.sendPhoto(chat_id=chat.id, photo=get_photo)
    context.bot.send_message(chat_id=chat.id, text=desc)
    


token = "5062671832:AAGMgq68i-ryuglOJEpxk6_GeFyNPmnOo38"

updater = Updater(token, use_context=True)

dispatcher = updater.dispatcher
dispatcher.add_handler(CommandHandler("start", on_start))
dispatcher.add_handler(MessageHandler(Filters.all, on_message))

updater.start_polling()
updater.idle()